import re

from datetime import datetime
from wxcloudrun.models import Subscribers

TEMPLATE_WELCOME='您是要订阅每周生命余额提醒吗？是的话，请回复YYYY.MM.DD格式的生日，比如说2000.01.01。查询订阅情况请回复“查询”。取消订阅请回复“取消”。'
TEMPLATE_SUBSCRIBED='您成功订阅了每周生命余额提醒。取消订阅请回复“取消”。'
TEMPLATE_UNSUBSCRIBED='您成功取消了每周生命余额提醒。'
TEMPLATE_QUERY='您订阅了每周生命余额提醒。您的生日是{}。取消订阅请回复“取消”。'
TEMPLATE_QUERY_NONE='您尚未订阅每周生命余额提醒。'

DATE_PATTERN = r'\b\d{4}\.\d{2}\.\d{2}\b'
DATE_FORMAT = "%Y.%m.%d"

def subscribe_handler(user_id, text):
    
    matches = re.findall(DATE_PATTERN, text)

    if (len(matches) > 0):
        birthday = datetime.strptime(matches[0], DATE_FORMAT)
        _create_birthday(user_id, birthday)
        return TEMPLATE_SUCCESS
    elif (text == '查询'):
        birthday = _get_birthday(user_id)
        if (birthday is not None):
            return TEMPLATE_QUERY.format(birthday.strftime(DATE_FORMAT))
        else:
            return TEMPLATE_QUERY_NONE
    elif (text == '取消'):
        _cancel_birthday(user_id)
        return TEMPLATE_UNSUBSCRIBED
    else:
        return TEMPLATE_WELCOME

def _get_birthday(user_id):
    try:
        subscriber = Subscribers.objects.get(user_id=user_id)
        return subscriber.birthday
    except Subscribers.DoesNotExist:
        return None

def _create_birthday(user_id, birthday):
    try:
        data = Subscribers.objects.get(user_id=user_id)
    except Subscribers.DoesNotExist:
        data = Subscribers()
    data.user_id = user_id
    data.birthday = birthday
    data.updatedAt = datetime.now()
    data.save()

def _cancel_birthday(user_id):
    try:
        data = Subscribers.objects.get(user_id=user_id)
        data.delete()
    except Subscribers.DoesNotExist:
        return
